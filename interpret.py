#!/usr/bin/env python3.6
#################################
#author:    Peter Lukac
#login:     xlukac11
#

import xml.etree.ElementTree as ET
import fileinput
import os.path
import sys
import argparse
import re

sys.path.insert(0, 'intLib')

import syntax
import execute
import decode
import frame

argSetUp = argparse.ArgumentParser(description='IPPcode18 interperter using xml input')
argSetUp.add_argument('--source=', type=str ,dest='file', required=True, metavar="FILE", help='path to the xml file')


try:
    arg = argSetUp.parse_args()
except SystemExit:
    sys.stderr.write("invalid arguments\n")
    sys.exit(10)

if not os.path.exists(arg.file):
    sys.stderr.write("there is no path to file: " + arg.file + "\n")
    sys.exit(11)

if not os.access(arg.file, os.R_OK):
    sys.stderr.write("can't read file: " + arg.file + "\n")
    sys.exit(12)

try:
    xmlInput = ET.parse(arg.file)
except Exception:
    sys.stderr.write("can't parse xml content\n")
    sys.exit(31)

program = xmlInput.getroot()

if not program.tag == "program":
    sys.stderr.write("program element is missing in the root\n")
    sys.exit(31)

if not program.attrib.get("language") == "IPPcode18":
    sys.stderr.write("language attribute is missing in the root\n")
    sys.exit(31)


xmlInstructions = list(program.iter("instruction"))

if not len(program.getchildren()) == len(xmlInstructions):
    sys.stderr.write("invalid instruction elements in the file\n")
    sys.exit(31)



programCounter = 1
memSpace = frame.FramesVarsStack("mem")

returnStack = []

labels = program.findall("instruction[@opcode='LABEL']")

labelMap = {}
for label in labels:
    labelArg = label.findall("arg1")
    labelOpcode = label.get("order")
    if len(labelArg) == 0:
        sys.stderr.write("invalid arg element for label\n")
        sys.exit(31)
    if len(labelArg) >= 2:
        sys.stderr.write("invalid too much arg elemnt for label\n")
        sys.exit(31)
    if labelArg[0].text in labelMap:
        sys.stderr.write("too many same labels: "+labelArg[0].text+"\n")
        sys.exit(31)
    for order in labelMap.values():
        if order == labelOpcode:
            sys.stderr.write("too labels with same order\n")
            sys.exit(31)
    labelMap[ labelArg[0].text ] = labelOpcode
    

while True:
    insList = program.findall("instruction[@order='"+str(programCounter)+"']")
    if len(insList) == 0:
        break

    if len(insList) >= 2:
        sys.stderr.write("too many instructions with same order\n")
        sys.exit(31)
        
    instruction = insList[0]
    #sys.stderr.write( instruction.get("opcode") + "\n" )
    programCounter = decode.decodeIns(returnStack, labelMap, memSpace, instruction, programCounter)

#memSpace.showAllVariables()
#memSpace.showStack()