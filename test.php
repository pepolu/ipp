#!/usr/bin/php
<?php
/****************************
*author:    Peter Lukac
*login:     xlukac11
**/

class test{
    public $src;
    public $out;
    public $rc;
    public $in;
}

$stderr = fopen('php://stderr', 'w');

function printBr($str){
    for($i = 0; $i < strlen($str); $i++){
        if($str[$i] == "\n")
            printf("<br>");
        printf("%s",$str[$i]);
    }
}

function findArg($args, $argName){
    for($i = 0; $i < count($args); $i++)
        if( $args[$i] ==  $argName)
            return $argName;
}

function getRecursiveDirs($dir){
    $diveCommand = "ls -ld " . $dir;
    $sortAWK = " | awk '{print $9}'";
    $dirArray = array();
    $rc = "0";
    while( $rc[0] == "0" ){
        $output = shell_exec( $diveCommand . " 2>/dev/null | awk '{print $9}'" );
        $rc = shell_exec( $diveCommand . " 1>/dev/null 2>/dev/null; echo $?" );
        if( $rc[0] == "0" ){
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $output) as $line){
                if($line != "\n" && $line != "")
                    array_push($dirArray, $line);
            }
        }
        $diveCommand = $diveCommand . "*/";
    }
    return $dirArray;
}

function getArgValue($args, $argName){
    global $stderr;
    for($i = 0; $i < count($args); $i++){
        if( preg_match( "/^" . $argName . ".*$/" , $args[$i] ,$match) === 1){
            if( strlen($argName) ===  strlen($args[$i]) ){
                $i++;
                if( $i == count($args) ){
                    fprintf($stderr,"no arg at: %s\n", $argName);
                    exit(10);
                }
                else
                    return $args[$i];
            }
            else if( $argName[strlen($argName)-1] === "=" ){
                return substr($args[$i], strlen($argName) );
            }
            else{
                return "";
            }
        }
    }
    return "";
}

$testDir = getArgValue($argv, "--directory=");
$parse = getArgValue($argv, "--parse-script=");
$int = getArgValue($argv, "--int-script=");
$rec = findArg($argv, "--recursive");
$help = findArg($argv, "--help");

if( $help == "--help" ){
    if( $testDir != "" || $parse != "" || $int != "" || $rec != ""){
        fprintf($stderr,"can't use any arg with help\n", $argName);
        exit(10);
    }
    printf("Testing script\n");
    exit(0);
}

if( $testDir == "" )
    $testDir = "./";
if( $parse == "" )
    $parse = "parse.php";
if( $int == "" )
    $int = "interpret.py";

if( $testDir[strlen($testDir)-1] != "/" )
    $testDir .= "/";

if( file_exists( $testDir ) == False ){
    fprintf($stderr,"failed to open file %s\n", $testDir);
    exit(10);
}

$dirArray = array();
if( $rec == "" ){
    array_push($dirArray, "" );
    $dirArray[0] = $testDir;
}
else if( $rec == "--recursive" )
    $dirArray = getRecursiveDirs($testDir);
else{
    fprintf($stderr,"error: --recursive param\n");
    exit(10);
}

printf("<html>\n<head>\n<meta name='viewport' content='width=device-width, initial-scale=1'>\n");
printf("<link rel='stylesheet' href='style.css'>\n");
printf("</head>\n");
printf("<body>\n<div class='main'>\n");

printf("<div class='head'>\n");
printf("<h1 id='result'>q</h1>\n");
printf("</div>\n");


$testCompleted = 0;
$testFailed = 0;

foreach( $dirArray as $line){
    fprintf($stderr,"%s\n",$line);

    $dirContent = scandir($line);
    $testPath = $line;

    $testArray = array();

    $testName = "";

    $testSrc = "";
    $testOut = "";
    $testRc = "";
    $testIn = "";

    for( $i=0; $i < count($dirContent); $i++ ){
        if( preg_match( "/^.+\.src$/" , $dirContent[$i] ,$match) === 1){
            $testSrc = $match[0];

            for( $j = strlen($testSrc) -1; $j > 0; $j-- )
                if( $testSrc[$j] == "." )
                    $testName = substr($testSrc, 0, $j );

            array_push($testArray, $testName);
        }
    }

    if( count($testArray) == 0 ){
        fprintf($stderr,"-- no test found\n\n");
        continue; 
    }

    $testCompleted++;
    fprintf($stderr,"%d tests in %s\n", count($testArray),$testPath );
    for($i = 0; $i < count($testArray); $i++){
        fprintf($stderr,"test name: %s\n",$testArray[$i]);
        $testName = $testArray[$i];
        $testSrc = $testName.".src";
        $testOut = "";
        $testRc = "";
        $testIn = "";
        for( $j=0; $j < count($dirContent); $j++ ){
            if( preg_match( "/^".$testName."\.out$/" , $dirContent[$j] ,$match) === 1)
                $testOut = $match[0];
            if( preg_match( "/^".$testName."\.rc$/" , $dirContent[$j] ,$match) === 1)
                $testRc = $match[0]; 
            if( preg_match( "/^".$testName."\.in$/" , $dirContent[$j] ,$match) === 1)
                $testIn = $match[0];
        }
        
        for( $j = strlen($testSrc) -1; $j > 0; $j-- )
            if( $testSrc[$j] == "." )
                $testName = substr($testSrc, 0, $j );

        if( $testOut == "" ){
            shell_exec("touch ".$testPath.$testName.".out");
            file_put_contents($testPath.$testName.".out", "");
            $testOut = $testName.".out";
        }
        if( $testIn == "" ){
            shell_exec("touch ".$testPath.$testName.".in");
            file_put_contents($testPath.$testName.".in", "");
            $testIn = $testName.".in";
        }
        if( $testRc == "" ){
            shell_exec("touch ".$testPath.$testName.".rc");
            file_put_contents($testPath.$testName.".rc", "0\n");
            $testRc = $testName.".rc";
        }
        $rc = file_get_contents($testPath.$testRc);
        if( $rc[strlen($rc)-1] == "\n" )
            $rc = substr($rc, 0, strlen($rc)-1);


        // RUN parse test

        $parseOut = shell_exec("php ".$parse." <".$testPath.$testSrc." 2>/dev/null");
        $parseRc = shell_exec("php ".$parse." <".$testPath.$testSrc." 1>/dev/null 2>/dev/null; echo $?");
        if( $parseRc[strlen($parseRc)-1] == "\n" )
            $parseRc = substr($parseRc, 0, strlen($parseRc)-1);
        //printf("rc len: %d\n",strlen($parseRc));

        // RUN int test if parse Ok
        printf("\t<div class='test'>\n");
        if( $parseRc != "0" ){
            if($parseRc == $rc){
                printf("\t\t<div class='testHead ok'>\n");
                printf("\t\t\t<h2>TEST: %s - OK</h2>\n", $testName);
                fprintf($stderr, "parse rc ok\n");
            }
            else{
                $testFailed++;
                printf("\t\t<div class='testHead fail'>\n");
                printf("\t\t\t<h2>TEST: %s FAILED</h2>\n", $testName);
                fprintf($stderr, "parse rc failes\n");
            }
            fprintf($stderr,"parser test failed, so end\n");
            printf("\t\t</div>\n");
            printf("\t\t<div class='testBody'>\n");
            printf("\t\t\t<p>%s returned code: %d, expected %d</p>\n", $parse, $parseRc, $rc);
            printf("\t\t</div>\n");
            
        }else{

            // let's save semicode, so we can interpret it
            $xmlFile = $testPath.$testName.".xml";
            shell_exec("touch ".$xmlFile);
            file_put_contents($xmlFile, $parseOut);

            // run interpret and colect output and return code
            $intOut = shell_exec("python3.6 ".$int." --source=".$xmlFile." < ".$testPath.$testIn." 2>/dev/null");
            $intRc = shell_exec("python3.6 ".$int." --source=".$xmlFile." < ".$testPath.$testIn." 2>/dev/null 1>/dev/null; echo $?");
            if( $intRc[strlen($intRc)-1] == "\n" )
                $intRc = substr($intRc, 0, strlen($intRc)-1);

            // .int file is output of interpret, compare with and save compare result in diff
            $intFile = $testPath.$testName.".int";
            shell_exec("touch ".$intFile);
            file_put_contents($intFile, $intOut);
            $diff = shell_exec("diff ".$intFile. " ".$testPath.$testOut);

            if($rc != $intRc)
                fprintf($stderr, "interpret return code fail\n");
            else
                fprintf($stderr, "interpret return code OK\n");

            //printf("%s:\n",$intOut);
            if( $diff == "")
                fprintf($stderr, "output ok\n");
            else
                fprintf($stderr, "diff: %s\n",$diff);

            shell_exec("rm ".$intFile);
            shell_exec("rm ".$xmlFile);
            fprintf($stderr,"\n");

            // generate html output
            if( ($rc == $intRc) &&  ($diff == "") ){
                printf("\t\t<div class='testHead ok'>\n");
                printf("\t\t\t<h2>TEST: %s - OK</h2>\n", $testName);
                printf("\t\t</div>\n");
            }else{
                $testFailed++;
                printf("\t\t<div class='testHead fail'>\n");
                printf("\t\t\t<h2>TEST: %s - FAILED</h2>\n", $testName);
                printf("\t\t</div>\n");
            }
            printf("\t\t<div class='testBody'>\n");
            printf("\t\tdir: %s<br>\n", $testPath);
            printf("\t\t\t%s returned code: %d<br>\n", $parse, $parseRc);
            printf("\t\t\t%s returned code: %d, expected %d<br>\n", $int, $intRc, $rc);
            if( $diff == "" ){
                printf("\t\t\tOutput OK\n");
            }
            else{
                printf("\t\t\tOutput FAIL\n");
            }
            // drop down show src

            // drop down show out
        
        }
        printf("\t</div>\n");
        printf("\t</div>\n"); // class test div
    }
}

fprintf($stderr,"all end\n");
printf("</div>\n"); // main div
printf("<script>
function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
r(function(){
    var x = document.getElementById(\"result\");
    x.innerText = \"%d Test Run. %d Test failed\";
});
</script>\n", $testCompleted, $testFailed);
printf("</body>\n</html>\n");

?>