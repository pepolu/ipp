<?php
/****************************
*@author:   Peter Lukac
*@author:   xlukac11
**/

$instructionWord = array(
    "MOVE",
    "CREATEFRAME",
    "PUSHFRAME",
    "POPFRAME",
    "DEFVAR",
    "CALL",
    "RETURN",
    "PUSHS",
    "POPS",
    "ADD",
    "SUB",
    "MUL",
    "IDIV",
    "LT",
    "GT",
    "EQ",
    "AND",
    "OR",
    "NOT",
    "INT2CHAR",
    "STRI2INT",
    "READ",
    "WRITE",
    "CONCAT",
    "STRLEN",
    "GETCHAR",
    "SETCHAR",
    "TYPE",
    "LABEL",
    "JUMP",
    "JUMPIFEQ",
    "JUMPIFNEQ",
    "DPRINT",
    "BREAK"
);

$constWord = array(
    "float@",
    "int@",
    "string@",
    "bool@"
);

$frameWord = array(
    "LF@",
    "TF@",
    "GF@"
);

$typeWord = array(
    "float",
    "int",
    "string",
    "bool"  
);



class token{
    public $tokenStr;
    public $type;
    public $isValid;
    function __construct($lex){
        if( isInstruction($lex) ){
            $this->tokenStr = $lex;
            $this->type = "opcode";
            $this->isValid = true;
        } else if( isVar($lex) ){
            $this->tokenStr = $lex;
            $this->type = "var";
            $this->isValid = true;
        } else if( isConst($lex) ){
            $this->type = getConstType($lex);
            $this->tokenStr = getConstData($lex);
            $this->isValid = true;
        } else if( isType($lex) ){
            $this->tokenStr = $lex;
            $this->type = "type";
            $this->isValid = true;
        }else if( isIndentificator($lex) ){
            $this->tokenStr = $lex;
            $this->type = "label";
            $this->isValid = true;
        }else{
            $this->tokenStr = $lex;
            $this->type = "unknownLexem";
            $this->isValid = false;
        }

    }
}


/*********
 * Simple function to strinp const data from const type
 * @param  $lex     const where data are stripped
 * @return  string  stripped const
 */
function getConstData($lex){
    $i = strpos($lex,"@");
    return substr($lex,$i+1);
}


/*******
 * Function identifies type of const
 * @param   $lex    lexem with type
 * @return  string  type in string
 */
function getConstType($lex){

    if( preg_match("/^int@.+/",$lex,$match) == 1)
        return "int";

    if( preg_match("/^bool@.+/",$lex,$match) == 1)
        return "bool";

    if( preg_match("/^float@.+/",$lex,$match) == 1)
        return "float";

    if( preg_match("/^string@.*/",$lex,$match) == 1)
        return "string";

    return "false";
}


/******************
 * Function that clasifies if lexem is keyword
 * @param $lex  lexem containing keyword or not
 * @return bool
 */
function isInstruction($lex){
    global $instructionWord;
    for($i = 0; $i < count($instructionWord); $i++){
        if(strcasecmp( $lex, $instructionWord[$i] ) === 0)
            return true;
    }
    return false;
}


/**********************
 * Function that clasifies if lexem is variable
 * @param $lex is lexem containing variable
 * @return bool
 */
function isVar($lex){

    if( preg_match("/^TF@[a-zA-Z_\-\$&%*]{1}[a-zA-Z0-9_\-\$&%*]*$/",$lex,$match) === 1 )
        return true;
    
    if( preg_match("/^LF@[a-zA-Z_\-\$&%*]{1}[a-zA-Z0-9_\-\$&%*]*$/",$lex,$match) === 1)
        return true;
    
    if( preg_match("/^GF@[a-zA-Z_\-\$&%*]{1}[a-zA-Z0-9_\-\$&%*]*$/",$lex,$match) === 1)
        return true;

    return false;
}


/****************************
 * Function that clasifies if lexem is const
 * @param $lex	lexem containing const
 * @return bool
 */
function isConst($lex){

    if( preg_match("/^int@[-+]{0,1}[0-9]+$/",$lex,$match) === 1 )
        return true;
    
    if( preg_match("/^bool@(true|false)$/",$lex,$match) === 1)
        return true;
    
    if( preg_match("/^float@.+/",$lex,$match) === 1)
        return true;

    if( preg_match("/^string@([!-\"$-\[\]-~\x80-\x9F\xA1-\xAC\xAE-\xFF]*(\\\\[0-9]{3})*)*$/",$lex,$match) === 1)
        return true;

    return false;
}


/***********************
 * Functions checks if lexem is identificator
 * @param $lex lexem containing identificator
 * @return bool
 */
function isIndentificator($lex){
    
    if( preg_match("/^[a-zA-Z_\-\$&%*]{1}[a-zA-Z0-9_\-\$&%*]*$/",$lex,$match) === 1)
        return true;

    return false;
}


/*************************
 * Function checks if lexem is type 
 * @param $lex lexem containing type
 * @return bool
 */
function isType($lex){
    global $typeWord;
    for($i = 0; $i < count($typeWord); $i++){
        if( strcmp($lex, $typeWord[$i]) === 0 )
            return true;
    }
    return false;
}


/****************************
 * Function prints lexems 
 * @param $lexArray array of lexems
 */
function printLexems($lexArray){
    global $stderr;
    for($i = 0; $i < count($lexArray); $i++){
        fprintf($stderr,"%s",$lexArray[$i]);
        if($i + 1 < count($lexArray))
            fprintf($stderr," | ");
    }
    fprintf($stderr,"\n");
}


/*************************
 * Function prints tokens
 * @param $lexArray array of tokens
 */
function printTokens($tokenArray){
    global $stderr;
    for($i = 0; $i < count($tokenArray); $i++){
        fprintf($stderr,"%s: %s",$tokenArray[$i]->type,$tokenArray[$i]->tokenStr);
        if($i + 1 < count($tokenArray))
            fprintf($stderr," | ");
    }
    fprintf($stderr,"\n\n");
}


/************************
 * Function cycles through lexems and creates array of objects, that represent
 * @param $lexArray array of lexems
 * @return array of tokens
 */
function tokeniseLexArray($lexArray){
    for($i = 0; $i < count($lexArray); $i++){
        $tokenArray[] = new token($lexArray[$i]); 
    }
    return $tokenArray;
}


/************************
 * Function parses line, based on spaces, tabs and recognizes comments
 * @param $line line to be parsed
 * @return array of lexems
 */
function parseLine($line){
    global $comCount;
    $i = 0;
    $lexIndex = 0;
    $lexArray = array();
    while($i < strlen($line)){
        if($line[$i] != ' ' && $line[$i] != "\t"){
            if($line[$i] === '#'){
                $comCount++;
                break;
            }

            array_push($lexArray,"");
            while($i < strlen($line) && $line[$i] != ' ' && $line[$i] != "\t"){
                if($line[$i] === '#'){
                    break;
                }

                $lexArray[$lexIndex] .= $line[$i];
                $i++;
            }
            $lexIndex++;
        }
        else if($line[$i] === '#'){
            $comCount++;
            break;
        }
        else
            $i++;
    }

    return $lexArray;
}


/************************
 * Function cycles through line and chcecks if contains interesting characters to be parsed
 * @param $line line to be checked 
 * @return bool
 */
function isCommentOrIsEmpty($line){
    global $comCount;
    if( strlen($line) === 0 )
        return true;

    $i = 0;
    while($line[$i] === ' ' || $line[$i] === "\t"){
        $i++;
        if( $i === strlen($line) )
            return true;
    }

    if($line[$i] === '#'){
        $comCount++;
        return true;
    }
    else
        return false;
}


/*****************************
 * Run through all token and check if all are valid tokens
 * @param $tokenArray array of tokens
 * @return bool
 */
function lexemCheck($tokenArray){
    for($i = 0; $i < count($tokenArray); $i++){
        if($tokenArray[$i]->isValid === false)
            return false;
    }
    return true;
}


/*****************************
 * Check if token is stack instruction
 * @param $token token
 * @return bool
 */
function isStackInstruction($token){
    $ins = array("CLEARS", "ADDS", "SUBS", "MULS", "IDIVS", "LTS", "GTS", "EQS", "ANDS", "ORS", "NOTS",
     "INT2CHARS", "STRI2INTS");
     foreach($ins as $ins){
        if(strcmp($ins, $token) === 0)
            return true;
     }
     return false;
}


/*****************************
 * Check if token is <var> <symb> <symb> instruction
 * @param $token token
 * @return bool
 */
function isVarSymbSymbInstruction($token){
    $ins = array("ADD", "SUB", "MUL", "IDIV", "LT", "GT", "EQ", "AND", "OR", "STRI2INT",
     "CONCAT", "GETCHAR", "SETCHAR");
     foreach($ins as $ins){
        if(strcmp($ins, $token) === 0)
            return true;
     }
     return false;
}


/*****************************
 * Check if token is <symb>
 * @param $token token
 * @return bool
 */
function isSymbol($token){
    $arg = array("var","string","int","float","bool");
    foreach($arg as $arg){
        if(strcmp($arg, $token) === 0)
            return true;
     }
     return false;
}


/*****************************
 * Check if token is only instruction
 * @param $token token
 * @return bool
 */
function isOnlyInstruction($token){
    $ins = array("CREATEFRAME","PUSHFRAME","POPFRAME","RETURN","BREAK");
     foreach($ins as $ins){
        if(strcmp($ins, $token) === 0)
            return true;
     }
     return false;
}


/*****************************
 * Perform syntax controll
 * @param $tokenArray array of tokens
 * @return bool
 */
function syntaxCheck($tokenArray){

    $tokenArray[0]->tokenStr = strtoupper($tokenArray[0]->tokenStr);

    // <var> <symb>, MOVE, TYPE
    if( $tokenArray[0]->tokenStr === "MOVE" || $tokenArray[0]->tokenStr === "TYPE" || $tokenArray[0]->tokenStr === "INT2CHAR" || $tokenArray[0]->tokenStr === "STRLEN" || $tokenArray[0]->tokenStr === "NOT"){
        if( count($tokenArray) !== 3 )
            return false;

        if($tokenArray[1]->type !== "var")
            return false;
        
        if(isSymbol($tokenArray[2]->type) === false)
                return false;

        return true;
    }

    // <var>, DEFVAR, POPS
    if( $tokenArray[0]->tokenStr === "DEFVAR" || $tokenArray[0]->tokenStr === "POPS" ){
        if( count($tokenArray) !== 2 )
            return false;

        if($tokenArray[1]->type !== "var")
            return false;

        return true;
    }

    // <symb>, DPRINT, WRITE, PUSHS
    if( $tokenArray[0]->tokenStr === "DPRINT" || $tokenArray[0]->tokenStr === "WRITE" || $tokenArray[0]->tokenStr === "PUSHS"){
        if( count($tokenArray) !== 2 )
            return false;

        if(isSymbol($tokenArray[1]->type) === false)
            return false;

        return true;
    }

    // <label>, CALL, LABEL, JUMP
    if( $tokenArray[0]->tokenStr === "CALL" || $tokenArray[0]->tokenStr === "LABEL" || $tokenArray[0]->tokenStr === "JUMP"){
        if( count($tokenArray) !== 2 )
            return false;

        if($tokenArray[1]->type !== "label" && $tokenArray[1]->type !== "type" && $tokenArray[1]->type !== "opcode")
            return false;

        return true;
    }

    // <var> <symb> <symb> ADD, SUB, MUL, IDIV, LT, GT, EQ, AND, OR, NOT, STRI2INT, CONCAT, GETCHAR, SETCHAR
    if( isVarSymbSymbInstruction($tokenArray[0]->tokenStr)){
        if( count($tokenArray) !== 4 )
            return false;

        if($tokenArray[1]->type !== "var")
            return false;
        
        if(isSymbol($tokenArray[2]->type) === false)
            return false;

        if(isSymbol($tokenArray[3]->type) === false)
            return false;

        return true;
    }

    // <label> <symb> <symb> JUMPIFEQ, JUMIFNEQ
    if( $tokenArray[0]->tokenStr === "JUMPIFEQ" || $tokenArray[0]->tokenStr === "JUMPIFNEQ" ){
        if( count($tokenArray) !== 4 )
            return false;

        if($tokenArray[1]->type !== "label" && $tokenArray[1]->type !== "type" && $tokenArray[1]->type !== "opcode")
            return false;
        
        if(isSymbol($tokenArray[2]->type) === false)
            return false;

        if(isSymbol($tokenArray[3]->type) === false)
            return false;

        return true;
    }

    // CREATEFRAME, PUSHFRAME, POPFRAME, RETURN, BREAK
    if( isOnlyInstruction($tokenArray[0]->tokenStr) ){
        if( count($tokenArray) === 1 )
            return true;
        else
            return false;
    }

    // CLEARS, ADDS, SUBS, ...
    if( isStackInstruction($tokenArray[0]->tokenStr) ){
        if( count($tokenArray) === 1 )
            return true;
        else
            return false;
    }

    // <label>, JUMPIFEQS, JUMPIFNEQS
    if( $tokenArray[0]->tokenStr === "JUMPIFEQS" || $tokenArray[0]->tokenStr === "JUMPIFNEQS" ){
        if( count($tokenArray) !== 2 )
            return false;

        if($tokenArray[1]->type !== "label" && $tokenArray[1]->type !== "type" && $tokenArray[1]->type !== "opcode")
            return false;

        return true;
    }

    if( $tokenArray[0]->tokenStr === "READ" ){
        if( count($tokenArray) !== 3 )
            return false;

        if($tokenArray[1]->type !== "var")
            return false;

        if($tokenArray[2]->type !== "type")
            return false;

        return true;
    }

    return false;
}


/*****************************
 * Aid instruction to get context of label nonterminal
 * @param $token token
 * @return bool
 */
function hasLabel($token){
    $ins = array("CALL","LABEL", "JUMP", "JUMPIFEQ", "JUMPIFNEQ" ,"JUMPIFEQS", "JUMPIFNEQS");
     foreach($ins as $ins){
        if(strcmp($ins, $token) === 0)
            return true;
     }
     return false;
}


/*****************************
 * Print xml output to stdout
 * @param $tokenArray array of tokens
 * @return bool
 */
function printXMLline($tokenArray){
    global $order;
    global $locCount;
    $locCount++;

    if( isOnlyInstruction($tokenArray[0]->tokenStr) ){
        printf("\t<instruction order=\"%d\" opcode=\"%s\"/>\n",$order,strtoupper($tokenArray[0]->tokenStr));
        $order++;
        return;
    }

    printf("\t<instruction order=\"%d\" opcode=\"%s\">\n",$order,strtoupper($tokenArray[0]->tokenStr));

    for($i = 1; $i < count($tokenArray);$i++){
        $cont = $tokenArray[$i]->tokenStr;
        $cont = str_replace("&","&amp;",$cont);
        $cont = str_replace("<","&lt;",$cont);
        $cont = str_replace(">","&gt;",$cont);
        if( hasLabel(strtoupper($tokenArray[0]->tokenStr)) === true && $i === 1 ){
            printf("\t\t<arg%d type=\"label\">%s</arg%d>\n",$i,$cont,$i);
        }
        else{
            printf("\t\t<arg%d type=\"%s\">%s</arg%d>\n",$i,$tokenArray[$i]->type,$cont,$i);
        }
    }
    $order++;
    printf("\t</instruction>\n\n");
}


/*****************************
 * Find index of string in string array based on keyword
 * @param $array arguments
 * @param $word word to be find
 * @return int
 */
function findStrInArray($array, $word){
    global $stderr;
    $ret = 0;
    for($i = 0; $i < sizeof($array); $i++){
        if( strcmp($array[$i], $word) === 0 ){
            if( $ret === 0 )
                $ret = $i;
            else{
                fprintf($stderr,"args are repeating\n");
                exit(10);
            }
        }
    }
    return $ret;
}


/*****************************
 * Gets filename from args
 * @param $array arguments
 * @return string
 */
function getFileFromArg($array){
    global $stderr;
    $str = "";
    for($i = 0; $i < sizeof($array); $i++){
        if( preg_match("/^--stats=.*$/",$array[$i] ,$match) === 1){
            if( $str === "" ){
                if( $array[$i] === "--stats=" ){
                    $i++;
                    if( $i ===  sizeof($array) ){
                        fprintf($stderr,"missing file for stats\n");
                        exit(10);
                    }
                    $str = $array[$i];
                }
                else{
                    $str = substr($array[$i],8);
                }
            }
            else{
                fprintf($stderr,"too much of --stats= args\n");
                exit(10);
            }
        }
    }
    return $str;
}


/*****************************
 * Checks validity of arguments
 * @param $array arguments
 * @param $isCom comment is present
 * @param $isLoc command is present
 * @param $isHelp help is present
 */
function argCheck($array, $isCom, $isLoc, $isHelp){
    global $stderr;
    $c = 1;
    for($i = 0; $i < sizeof($array); $i++){
        if( preg_match("/^--stats=.*$/",$array[$i] ,$match) === 1){
            if( $array[$i] === "--stats=" )
                $c+=2;
            else
                $c++;
        }
    }
    if( $isCom != 0 )
        $c++;
    if( $isLoc != 0 )
        $c++;
    if( $isHelp != 0 )
        $c++;

    if( $c != sizeof($array) ){
        fprintf($stderr,"unknown args\n");
        exit(10);
    }
}


//
// START HERE
//
$order = 1;
$lineNum = 0;
$exitCode = 0;
$stderr = fopen('php://stderr', 'w');

$comCount = 0;
$locCount = 0;

$isCom = 0;
$isLoc = 0;
$isHelp = 0;
$file = NULL;

if( sizeof($argv) > 1 ){
    $isCom = findStrInArray($argv, "--comments");
    $isLoc = findStrInArray($argv, "--loc");
    $isHelp = findStrInArray($argv, "--help");
    $file = getFileFromArg($argv);
    
    if( ($isCom != 0 || $isLoc != 0) && $file === "" ){
        fprintf($stderr,"missing file\n");
        exit(10);
    }
    argCheck($argv, $isCom, $isLoc, $isHelp);
}

if( $isHelp != 0 ){
    if( sizeof($argv) > 2 ){
        fprintf($stderr,"can't combine help with anything else\n");
        exit(10);
    }
    printf("This script takes IPPcode18 source code from stdin and translates it into xml form ready to be interpreted\n");
    printf("parse.php only performs syntax controll of IPPcode18\n");
    printf("Example usage: php parse.php < [sourceFile] > [outputFile.xml]\n");
    exit(0);
}


do{
    $line = trim(fgets(STDIN));
    $lineNum++;
}while( isCommentOrIsEmpty($line) && ! feof(STDIN));

if( strlen($line) === 0 ){
    fprintf($stderr,"empty code\n");
    exit(0);
}

if(strcasecmp( $line, ".IPPcode18" )){
    fprintf($stderr,"\x1b[31merror: mmissing .IPPcode18 header\x1b[0m\n",$lineNum);
    exit(21);
}

printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
printf("<program language=\"IPPcode18\">\n");

while( ! feof(STDIN) ){
    $line = trim(fgets(STDIN));

    if(isCommentOrIsEmpty($line)){
        $lineNum++;
        continue;
    }
    
    $lineNum++;
    
    $lexArray = parseLine($line);
    printLexems($lexArray);

    $tokenArray = tokeniseLexArray($lexArray);
    printTokens($tokenArray);

    if( lexemCheck($tokenArray) === false){
        fprintf($stderr,"\x1b[31mlexem error at line %d\x1b[0m\n",$lineNum);
        $exitCode = 21;
    } else if( syntaxCheck($tokenArray) === false ){
        fprintf($stderr,"\x1b[31msyntax error at line %d\x1b[0m\n",$lineNum);
        $exitCode = 21;
    }
    
    printXMLline($tokenArray);
}

printf("</program>");

if( $file != "" ){

    $stat = fopen($file, 'w');
    if( $stat == 0 ){
        fprintf($stderr,"cant open file for --stats=\n");
        exit(12);
    }
    if( $isLoc != 0 && $isCom == 0 )
        fprintf($stat,"%d", $locCount);
    else if( $isLoc == 0 && $isCom != 0 )
        fprintf($stat,"%d", $comCount);
    else if($isLoc != 0 && $isCom != 0){
        if( $isLoc < $isCom )
            fprintf($stat,"%d\n%d", $locCount, $comCount);
        if( $isLoc > $isCom )
            fprintf($stat,"%d\n%d", $comCount, $locCount);
    }
    fclose($stat);
}

fprintf($stderr,"\x1b[32m\nstderr: program exited normaly\nloc: %d\ncom: %d\x1b[0m\n", $locCount, $comCount);
fclose($stderr);
exit($exitCode);
?>
