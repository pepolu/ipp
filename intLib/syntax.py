#################################
#author:    Peter Lukac
#login:     xlukac11
#


import xml.etree.ElementTree as ET
import re
import sys

def validateArgCount(instruction, count):
    """
    Check number of child elemntr in instruction
    """
    childrenCount = len(instruction.getchildren())

    if count == 0 and childrenCount == 0:
        return
    elif count == 0 and not childrenCount == 0:
        sys.stderr.write("arguments in non arg instruction\n")
        sys.exit(31)
    elif not count == childrenCount:
        sys.stderr.write("arg count doesn't match\n")
        sys.exit(31)

    for i in range(1, count):
        args = instruction.findall(".//arg"+str(i))
        if len(args) == 0:
            sys.stderr.write("invalid arg element: "+str(i)+"\n")
            sys.exit(31)
        if len(args) >= 2:
            sys.stderr.write("too many same arg elements: "+str(i)+"\n")
            sys.exit(31)
    return



def getArgType(instruction, index):
    """
    Return attribute of argument at index, indexed from 1
    """
    args = instruction.findall(".//arg"+str(index))
    if len(args) == 0:
        sys.stderr.write("invalid arg element: "+str(index)+"\n")
        sys.exit(31)
    if len(args) >= 2:
        sys.stderr.write("too many same arg elements: "+str(index)+"\n")
        sys.exit(31)

    return args[0].get("type")



def isVar(instruction, index):
    """
    Check if element attribute is var
    """
    args = instruction.findall(".//arg"+str(index))
    if len(args) == 0:
        sys.stderr.write("invalid arg element: "+str(index)+"\n")
        sys.exit(31)
    if len(args) >= 2:
        sys.stderr.write("too many same arg elements: "+str(index)+"\n")
        sys.exit(31)

    if args[0].get("type") == "var":
        return True
    else:
        return False



def isSymb(instruction, index):
    """
    Check if element attribute is something of a symb
    """
    args = instruction.findall(".//arg"+str(index))
    if len(args) == 0:
        sys.stderr.write("invalid arg element: "+str(index)+"\n")
        sys.exit(31)
    if len(args) >= 2:
        sys.stderr.write("too many same arg elements: "+str(index)+"\n")
        sys.exit(31)

    if args[0].get("type") == "var" or args[0].get("type") == "string" or args[0].get("type") == "float" or args[0].get("type") == "int" or args[0].get("type") == "bool":
        return True
    else:
        return False



def isLabel(instruction, index):
    """
    Check if element attribute is label
    """
    args = instruction.findall(".//arg"+str(index))
    if len(args) == 0:
        sys.stderr.write("invalid arg element: "+str(index)+"\n")
        sys.exit(31)
    if len(args) >= 2:
        sys.stderr.write("too many same arg elements: "+str(index)+"\n")
        sys.exit(31)

    if args[0].get("type") == "label":
        return True
    else:
        return False



def isType(instruction, index):
    """
    Check if element attribute is type
    """
    args = instruction.findall(".//arg"+str(index))
    if len(args) == 0:
        sys.stderr.write("invalid arg element: "+str(index)+"\n")
        sys.exit(31)
    if len(args) >= 2:
        sys.stderr.write("too many same arg elements: "+str(index)+"\n")
        sys.exit(31)

    if args[0].get("type") == "type":
        return True
    else:
        return False



def getFrame(instruction, index):
    """
    Check if element content contains variable and return frame of variable
    """
    arg = instruction.findall(".//arg"+str(index))
    frame = arg[0].text[:3]
    if frame == "GF@" or frame == "LF@" or frame == "TF@":
        return frame
    else:
        sys.stderr.write("unknown frame\n")
        sys.exit(32)



def getVarName(instruction, index):
    """
    Check if element content contains variable and return name of it
    """
    arg = instruction.findall(".//arg"+str(index))
    name = arg[0].text[3:]
    if re.match("[a-zA-Z_\-\$&%*]{1}[a-zA-Z0-9_\-\$&%*]*$", name):
        return name
    else:
        sys.stderr.write("invalid syntax in var name\n")
        sys.exit(32)





def getInteger(instruction, index):
    """
    Get integer from element content and perform syntax check
    """
    arg = instruction.findall(".//arg"+str(index))
    integer = arg[0].text
    if integer is None:
        sys.stderr.write("empty arg elemnt for integer\n")
        sys.exit(32)
    if re.match( "[-+]{0,1}[0-9]+$", integer):
        return integer
    else:
        sys.stderr.write("invalid syntax in integer const\n")
        sys.exit(32)



def getString(instruction, index):
    """
    Get string from element content and perform syntax check
    """
    arg = instruction.findall(".//arg"+str(index))
    string = arg[0].text
    if string is None:
        return ""
    if re.match(  r"([!-\"$-\[\]-~]*(\134[0-9]{3})*)*$", string):
        return string
    else:
        sys.stderr.write("invalid syntax in string const\n")
        sys.exit(32)



def getFloat(instruction, index):
    """
    Get float from element content and perform syntax check
    """
    arg = instruction.findall(".//arg"+str(index))
    floatConst = arg[0].text
    if integer is None:
        sys.stderr.write("empty arg elemnt for float\n")
        sys.exit(32)
    return floatConst


def getBool(instruction, index):
    """
    Get bool from element content and perform syntax check
    """
    arg = instruction.findall(".//arg"+str(index))
    type1 = arg[0].text
    if type1 is None:
        sys.stderr.write("empty arg elemnt for type\n")
        sys.exit(32)
    if re.match(  r"(true|false)$", type1):
        return type1
    else:
        sys.stderr.write("invalid syntax for bool\n")
        sys.exit(32)


def getType(instruction, index):
    """
    Get type from element content and perform syntax check
    """
    arg = instruction.findall(".//arg"+str(index))
    type1 = arg[0].text
    if type1 is None:
        sys.stderr.write("empty arg elemnt for type\n")
        sys.exit(32)
    if re.match(  r"(int|bool|string|float)$", type1):
        return type1
    else:
        sys.stderr.write("invalid syntax for type\n")
        sys.exit(32)




def getConstValue(instruction, index):
    """
    Get value from constant
    """
    arg = instruction.findall(".//arg"+str(index))
    type1 = arg[0].get("type")
    if type1 == "string":
        return getString(instruction, index)
    if type1 == "int":
        return getInteger(instruction, index)
    if type1 == "float":
        return getFloat(instruction, index)
    if type1 == "bool":
        return getBool(instruction, index)
