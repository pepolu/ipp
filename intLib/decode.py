#################################
#author:    Peter Lukac
#login:     xlukac11
#

import execute
import syntax
import sys


def resolveJumpPc(ins, labelMap):
    """
    Find instruction order, where to jump
    :ins        -- xml element containing entire instruction
    :labelMap   -- associative array containing labels to jump to
    """
    label = ins.findall("arg1")
    label = label[0].text
    if not label in labelMap:
        sys.stderr.write("jump to nonexisting label\n")
        sys.exit(31)
    return int(labelMap[label])

def decodeIns(returnStack, labelMap, memSpace, instruction, programCounter):
    """
    Decode instruction, perform xml syntax check and xml arguments check
    Execute instruction based on opcode
    :returnStack    -- stack of instruction order where we jumped from
    :labelMap       -- associative array containing labels to jump to
    :memSpace       -- object holding all variables and stack
    :instruction    -- xml element containing entire instruction
    :programCounter -- order of current onstruction
    """

    if instruction.get("opcode") == "MOVE":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        execute.move(memSpace, instruction)

    elif instruction.get("opcode") == "CREATEFRAME":
        syntax.validateArgCount(instruction, 0)
        memSpace.createFrame()

    elif instruction.get("opcode") == "PUSHFRAME":
        syntax.validateArgCount(instruction, 0)
        memSpace.pushFrame()

    elif instruction.get("opcode") == "POPFRAME":
        syntax.validateArgCount(instruction, 0)
        memSpace.popFrame()

    elif instruction.get("opcode") == "DEFVAR":
        syntax.validateArgCount(instruction, 1)
        syntax.isVar(instruction, 1)
        execute.defvar(memSpace, instruction)
        
    elif instruction.get("opcode") == "CALL":
        syntax.validateArgCount(instruction, 1)
        syntax.isLabel(instruction, 1)
        returnStack.append(programCounter)
        programCounter = resolveJumpPc(instruction, labelMap)
        
    elif instruction.get("opcode") == "RETURN":
        syntax.validateArgCount(instruction, 0)
        if len(returnStack) == 0:
            sys.stderr.write("return to nonexisting label\n")
            sys.exit(56)
        else:
            programCounter = returnStack.pop()


    elif instruction.get("opcode") == "PUSHS":
        syntax.validateArgCount(instruction, 1)
        syntax.isSymb(instruction, 1)
        execute.stackPush(memSpace, instruction)

    elif instruction.get("opcode") == "POPS":
        syntax.validateArgCount(instruction, 1)
        syntax.isSymb(instruction, 1)
        execute.stackPop(memSpace, instruction)
    

    elif instruction.get("opcode") == "ADD":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "+")
    elif instruction.get("opcode") == "SUB":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "-")
    elif instruction.get("opcode") == "MUL":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "*")
    elif instruction.get("opcode") == "IDIV":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "/")
    elif instruction.get("opcode") == "LT":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "lt")
    elif instruction.get("opcode") == "GT":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "gt")
    elif instruction.get("opcode") == "EQ":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "eq")
    elif instruction.get("opcode") == "AND":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "and")
    elif instruction.get("opcode") == "OR":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "or")
    elif instruction.get("opcode") == "NOT":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        execute.doOperation(memSpace, instruction, "not")
    elif instruction.get("opcode") == "INT2CHAR":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        execute.doOperation(memSpace, instruction, "i2c")
    elif instruction.get("opcode") == "STRI2INT":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.doOperation(memSpace, instruction, "s2i")

    
    elif instruction.get("opcode") == "READ":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isType(instruction, 2)
        execute.readOp(memSpace, instruction)
    elif instruction.get("opcode") == "WRITE":
        syntax.validateArgCount(instruction, 1)
        syntax.isSymb(instruction, 1)
        execute.writeSymb(memSpace, instruction)


    elif instruction.get("opcode") == "CONCAT":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.stringOp(memSpace, instruction, "+")
    elif instruction.get("opcode") == "STRLEN":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        execute.stringOp(memSpace, instruction, "len")
    elif instruction.get("opcode") == "GETCHAR":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.stringOp(memSpace, instruction, "get")
    elif instruction.get("opcode") == "SETCHAR":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        execute.stringOp(memSpace, instruction, "set")


    elif instruction.get("opcode") == "TYPE":
        syntax.validateArgCount(instruction, 2)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        execute.saveType(memSpace, instruction)


    elif instruction.get("opcode") == "LABEL":
        syntax.validateArgCount(instruction, 1)
        syntax.isLabel(instruction, 1)
    elif instruction.get("opcode") == "JUMP":
        syntax.validateArgCount(instruction, 1)
        syntax.isLabel(instruction, 1)
        programCounter = resolveJumpPc(instruction, labelMap)
    elif instruction.get("opcode") == "JUMPIFEQ":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        eq = execute.isEq(memSpace, instruction)
        if eq == True:
            programCounter = resolveJumpPc(instruction, labelMap)
    elif instruction.get("opcode") == "JUMPIFNEQ":
        syntax.validateArgCount(instruction, 3)
        syntax.isVar(instruction, 1)
        syntax.isSymb(instruction, 2)
        syntax.isSymb(instruction, 3)
        eq = execute.isEq(memSpace, instruction)
        if eq == False:
            programCounter = resolveJumpPc(instruction, labelMap)


    elif instruction.get("opcode") == "DPRINT":
        syntax.validateArgCount(instruction, 1)
        syntax.isSymb(instruction, 1)
        execute.dPrint(memSpace, instruction)
    elif instruction.get("opcode") == "BREAK":
        syntax.validateArgCount(instruction, 0)
        memSpace.showAllVariables()
        memSpace.showStack()
    else:
        sys.stderr.write("unknown instruction\n")
        sys.exit(32)

    programCounter += 1

    return programCounter