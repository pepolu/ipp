#################################
#author:    Peter Lukac
#login:     xlukac11
#

import syntax
import frame
import sys
import re


def getSymbType(memSpace, ins, index):
    type1 = syntax.getArgType(ins, index)
    if type1 == "var":
        varFrame = syntax.getFrame(ins, index)
        varName = syntax.getVarName(ins, index)
        type1 = memSpace.getVarType(varFrame, varName)
    return type1


def getSymbValue(memSpace, ins, index):
    type1 = syntax.getArgType(ins, index)
    if type1 == "var":
        varFrame = syntax.getFrame(ins, index)
        varName = syntax.getVarName(ins, index)
        return memSpace.getVarValue(varFrame, varName)
    elif type1 == "int":
        return int(syntax.getInteger(ins, index))
    elif type1 == "float":
        return syntax.getFloat(ins, index)
    elif type1 == "bool":
        return syntax.getBool(ins, index)
    elif type1 == "string":
        string = syntax.getString(ins, index)
        while len(re.findall( r"\134[0-9]{3}", string ) ) != 0:
            esc = re.findall(r"\134[0-9]{3}", string)[0]
            newChar = chr(int(esc[1:]))
            string = string.replace(esc,newChar,1)
        return string


def saveInVar(ins, value, type1, index):
    frame = syntax.getFrame(ins, index)
    name = syntax.getVarName(ins, index)
    memSpace.moveConstToVariable(frame, type1, name, value)

def move(memSpace, ins):
    frame = syntax.getFrame(ins, 1)
    destName = syntax.getVarName(ins, 1)
    #type1 = syntax.getArgType(ins, 1)
    #type2 = syntax.getArgType(ins, 2)
    '''
    if type2 == "var":
        varFrame = syntax.getFrame(ins, 2)
        varName = syntax.getVarName(ins, 2)

        varType = memSpace.getVarType(varFrame, varName)
        varValue = memSpace.getVarValue(varFrame, varName)
        
        memSpace.moveConstToVariable(frame, varType, destName, varValue)
    else:
        constType = syntax.getArgType(ins, 2)
        constValue = syntax.getConstValue(ins, 2)
        memSpace.moveConstToVariable(frame, constType, destName, constValue)
    '''
    type2 = getSymbType(memSpace, ins, 2)
    value2 = getSymbValue(memSpace, ins, 2)
    memSpace.moveConstToVariable(frame, type2, destName, value2)

def defvar(memSpace, ins):
    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)
    memSpace.declareVariable(frame, name)


def stackPush(memSpace, ins):
    type1 = syntax.getArgType(ins, 1)

    if type1 == "var":
        frame = syntax.getFrame(ins, 1)
        name = syntax.getVarName(ins, 1)

        varType = memSpace.getVarType(frame, name)
        varValue = memSpace.getVarValue(frame, name)
        memSpace.stackPush(varValue, varType)
    else:
        constType = syntax.getArgType(ins, 1)
        constValue = syntax.getConstValue(ins, 1)
        memSpace.stackPush(constValue, constType)


def stackPop(memSpace, ins):
    type1 = memSpace.stackTopType()
    value = memSpace.stackTopValue()

    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)

    memSpace.moveConstToVariable(frame, type1, name, value)

    memSpace.stackPop()


def isEq(memSpace, ins):
    type1 = getSymbType(memSpace, ins, 2)
    type2 = getSymbType(memSpace, ins, 3)

    value1 = getSymbValue(memSpace, ins, 2)
    value2 = getSymbValue(memSpace, ins, 3)

    if type1 != type2:
        sys.stderr.write("incompateble types\n")
        exit(53)

    if value1 == value2:
        return True
    else:
        return False
    

def writeSymb(memSpace, ins):
    type1 = getSymbType(memSpace, ins, 1)
    value1 = getSymbValue(memSpace, ins, 1)
    print( value1 )
    #if type1 == "string":
        #sys.stdout.write( value1 )
    #else:
        #sys.stdout.write( str(value1) )


def doOperation(memSpace, ins, op):
    type1 = getSymbType(memSpace, ins, 2)
    value1 = getSymbValue(memSpace, ins, 2)

    if type1 == "bool":
        if value1 == "true":
            value1 = 1
        else:
            value1 = 0

    if op != "not" and op != "i2c":
        type2 = getSymbType(memSpace, ins, 3)
        value2 = getSymbValue(memSpace, ins, 3)
        if type2 == "bool":
            if value2 == "true":
                value2 = 1
            else:
                value2 = 0

    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)

    """
    if type1 != type2:
        sys.stderr.write("incompateble types\n")
        exit(40)
    """

    if op == "+":
        if type1 == "int" and type2 == "int":
            value1 += value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        elif type1 == "float" and type2 == "float":
            value1 += value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        else:
            sys.stderr.write("incompateble types in add operation\n")
            exit(53)
    elif op == "-":
        if type1 == "int" and type2 == "int":
            value1 -= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        elif type1 == "float" and type2 == "float":
            value1 -= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        else:
            sys.stderr.write("incompateble types in sub operation\n")
            exit(53)
    elif op == "*":
        if type1 == "int" and type2 == "int":
            value1 *= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        elif type1 == "float" and type2 == "float":
            value1 *= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        else:
            sys.stderr.write("incompateble types in mul operation\n")
            exit(53)
    elif op == "/":
        if type1 == "int" and type2 == "int":
            if value2 == 0:
                sys.stderr.write("division by zero\n")
                exit(57)
            value1 //= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        elif type1 == "float" and type2 == "float":
            if value2 == 0:
                sys.stderr.write("division by zero\n")
                exit(57)
            value1 /= value2
            memSpace.moveConstToVariable(frame, type1, name, value1)
        else:
            sys.stderr.write("incompateble types in div operation\n")
            exit(40)
    elif op == "lt":
        if type1 != type2:
            sys.stderr.write("incompateble types in lt operation\n")
            exit(53)
        if value1 < value2:
            memSpace.moveConstToVariable(frame, "bool", name, "true")
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false")
    elif op == "gt":
        if type1 != type2:
            sys.stderr.write("incompateble types in gt operation\n")
            exit(40)
        if value1 > value2:
            memSpace.moveConstToVariable(frame, "bool", name, "true")
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false")
    elif op == "eq":
        if type1 != type2:
            sys.stderr.write("incompateble types in eq operation\n")
            exit(53)
        if value1 == value2:
            memSpace.moveConstToVariable(frame, "bool", name, "true")
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false")
    elif op == "and":
        if type1 != "bool" and type2 != "bool":
            sys.stderr.write("not bool types in and operation\n")
            exit(53)
        if value1 == 1 and value2 == 1:
            memSpace.moveConstToVariable(frame, "bool", name, "true" )
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false" )
    elif op == "or":
        if type1 != "bool" and type2 != "bool":
            sys.stderr.write("not bool types in or operation\n")
            exit(53)
        if value1 == 1 or value2 == 1:
            memSpace.moveConstToVariable(frame, "bool", name, "true" )
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false" )
    elif op == "not":
        if type1 != "bool":
            sys.stderr.write("not bool types in not operation\n")
            exit(53)
        if value1 == 0:
            memSpace.moveConstToVariable(frame, "bool", name, "true" )
        else:
            memSpace.moveConstToVariable(frame, "bool", name, "false" )
    elif op == "i2c":
        if type1 != "int":
            sys.stderr.write("not int type in i2c operation\n")
            exit(53)
        if value1 > 255 or value1 < 0:
            sys.stderr.write("i2c operand out of range\n")
            exit(58)
        memSpace.moveConstToVariable(frame, "string", name, chr(value1) )
    elif op == "s2i":
        if type1 != "string":
            sys.stderr.write("not string in s2i operation\n")
            exit(53)
        if type2 != "int":
            sys.stderr.write("not int in s2i operation\n")
            exit(53)
        if value2 < 0 or value2 >= len(value1):
            sys.stderr.write("s2i out of range\n")
            exit(58)
        memSpace.moveConstToVariable(frame, "int", name, ord(value1[value2]) )



def readOp(memSpace, ins):
    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)

    type1 = syntax.getType(ins, 2)

    data = input()

    if type1 == "int":
        try:
            data = int(data)
        except:
            data = 0
    elif type1 == "float":
        try:
            data = float(data)
        except:
            data = 0.0
    elif type1 == "bool":
        if re.match(r"[tT][rR][uU][eE]$", data):
            data = "true"
        else:
            data = "false"

    memSpace.moveConstToVariable(frame, type1, name, data)

def saveType(memSpace, ins):
    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)

    type1 = getSymbType(memSpace, ins, 2)

    if type1 == "undefinied":
        type1 = ""

    memSpace.moveConstToVariable(frame, "string", name, type1)


def stringOp(memSpace, ins, op):
    type1 = getSymbType(memSpace, ins, 2)
    value1 = getSymbValue(memSpace, ins, 2)

    if op != "len":
        type2 = getSymbType(memSpace, ins, 3)
        value2 = getSymbValue(memSpace, ins, 3)

    frame = syntax.getFrame(ins, 1)
    name = syntax.getVarName(ins, 1)

    if op == "+":
        if type1 != "string" and type2 != "string":
            sys.stderr.write("not string operand int concat\n")
            exit(58)
        memSpace.moveConstToVariable(frame, "string", name, value1 + value2 )
    elif op == "len":
        if type1 != "string":
            sys.stderr.write("not string operand int len\n")
            exit(58)
        memSpace.moveConstToVariable(frame, "int", name, len(value1) )
    elif op == "get":
        if type1 != "string":
            sys.stderr.write("not string in strget operation\n")
            exit(53)
        if type2 != "int":
            sys.stderr.write("not int in strget operation\n")
            exit(53)
        if value2 < 0 or value2 >= len(value1):
            sys.stderr.write("s2i out of range\n")
            exit(58)
        memSpace.moveConstToVariable(frame, "string", name, value1[value2] )
    elif op == "set":
        if type1 != "int":
            sys.stderr.write("not int in setchar operation\n")
            exit(53)
        if type2 != "string":
            sys.stderr.write("not string in setchar operation\n")
            exit(53)
        var = getSymbValue(memSpace, ins, 1)
        if value1 < 0 or value1 >= len(var):
            sys.stderr.write("out of range in setchar operation\n")
            exit(58)
        if len(value2) == 0:
            sys.stderr.write("empty source in setchar operation\n")
            exit(58)
        newStr = var[:value1]
        newStr += value2[0]
        newStr += var[value1+1:]
        memSpace.moveConstToVariable(frame, "string", name, newStr )

        


def dPrint(memSpace, ins):
    type1 = getSymbType(memSpace, ins, 2)
    value1 = getSymbValue(memSpace, ins, 2)

    if type1 != "string":
        value1 = str(value1)

    sys.stderr.write(value1)

    

