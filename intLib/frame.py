#################################
#author:    Peter Lukac
#login:     xlukac11
#

import sys
import copy

class Variable:
    """
    Simple class that holds variable type, name, declaration and value
    """
    name = ""
    value = 0
    definiedType = ""
    definied = False

    def __init__(self, name):
        self.name = name
        self.value = 0
        self.definiedType = ""
        self.definied = False

class Const:
    """
    Simple class that holds const as type with value
    """
    value = 0
    definiedType = ""

    def __init__(self, value, definiedType):
        self.value = value
        self.definiedType = definiedType


class VariableFrame:
    """
    Holds set of variables for a frame
    """
    variables = []
    name = ""

    def __init__(self, name):
        self.name = name
        self.variables = []


    def addSymbol(self, name):
        """
        Adds new variable, called at DEFVAR, chcecks for multiple declarations
        """
        for var in self.variables:
            if var.name == name:
                sys.stderr.write("redeclaration of variable: "+name+"\n")
                exit(52)
        self.variables.append(Variable(name))


    def setSymbol(self, name, newType, value):
        """
        Asign value to variable
        Perform semantic constrol - redefinition
        name    -- name of variable
        newType -- type of value
        value   -- value
        """
        for var in self.variables:
            if var.name == name:
                if var.definiedType == "":
                    var.definiedType = newType
                    if newType == "int":
                        var.value = int(value) # new bad boys, watch out for them
                    else:
                        var.value = value[:]
                    var.definied = True
                elif var.definiedType == newType:
                    if newType == "int":
                        var.value = int(value)
                    else:
                        var.value = value[:]
                else:
                    sys.stderr.write("retyping some variable: "+name+"\n")
                    exit(53)
                return
        sys.stderr.write("setting undeclared variable: "+name+"\n")
        exit(54)


    def getSymbolValue(self, name):
        """
        Return value of variable
        :name   -- name of variable
        """
        for var in self.variables:
            if var.name == name:
                if var.definied == False:
                    sys.stderr.write("can't get value undefinied variable: "+name+"\n")
                    exit(56)
                else:
                    return var.value
        sys.stderr.write("trying to read value of undeclared variable")
        exit(54)


    def getSymbolType(self, name):
        """
        Return type of variable
        :name   -- name of variable
        """
        for var in self.variables:
            if var.name == name:
                if var.definied == False:
                    return "undefinied" # watch out for this bad boy
                else:
                    return var.definiedType
        sys.stderr.write("trying to read type of undeclared variable")
        exit(54)



class FramesVarsStack:
    GF = VariableFrame("GF")
    LF = []
    TF = VariableFrame("TF")
    stack = []
    name = ""
    TFdefinied = False

    def __init__(self, name):
        self.GF = VariableFrame("GF")
        self.TF = VariableFrame("TF")
        self.name = name
        self.LF = []
        self.TFdefinied = False
        self.stack = []


    def showAllVariables(self):
        """
        Print content of memspace to stderr
        """
        sys.stderr.write("\nGLOBAL FRAME:\n")
        for var in self.GF.variables:
            sys.stderr.write("\t"+var.name+"\ttype:"+var.definiedType+"\t"+str(var.value)+"\n")
        sys.stderr.write("LOCAL FRAME:\n")
        for i, frame in enumerate(self.LF):
            sys.stderr.write("\tFRAME "+ str(i) +":\n")
            for var2 in frame.variables:
                sys.stderr.write("\t\t"+var2.name+"\ttype:"+var2.definiedType+"\t"+str(var2.value)+"\n")
        sys.stderr.write("TEMPORARY FRAME:\n")
        sys.stderr.write("definied " + str(self.TFdefinied) + "\n")
        for var1 in self.TF.variables:
            sys.stderr.write("\t"+var1.name+"\ttype:"+var1.definiedType+"\t"+str(var1.value)+"\n")
        sys.stderr.write("\n")

    
    def showStack(self):
        """
        Print content of memspace to stderr
        """
        for i, con in enumerate(self.stack):
            sys.stderr.write( str(i) + ": " + str(con.value) + "\t" + str(con.definiedType) + "\n" )


    def declareVariable(self, frame, name):
        """
        Add variable to frame
        """
        if frame == "GF@":
            self.GF.addSymbol(name)
        elif frame == "LF@":
            if len(self.LF) == 0:
                sys.stderr.write("there is no local frame in frame stack\n")
                exit(55)
            else:
                self.LF[len(self.LF)-1].addSymbol(name)
        elif frame == "TF@":
            if self.TFdefinied == False:
                sys.stderr.write("Working with undefinied TF\n");
                exit(55)
            else:
                self.TF.addSymbol(name)
        else:
            sys.stderr.write("trying to declare var to unknown frame: "+name+" "+frame)
            exit(70)


    def moveConstToVariable(self, frame, type1, name, value):
        """
        Save value in variable
        """
        if frame == "GF@":
            self.GF.setSymbol(name, type1, value)
        elif frame == "LF@":
            if len(self.LF) == 0:
                sys.stderr.write("there is no local frame in frame stack\n")
                exit(55)
            else:
                self.LF[len(self.LF)-1].setSymbol(name, type1, value)
        elif frame == "TF@":
            self.TF.setSymbol(name, type1, value)
        else:
            sys.stderr.write("trying to add var to unknown frame: "+name+" "+frame)
            exit(70)


    def getVarType(self, frame, name):
        """
        Get type of variable
        """
        if frame == "GF@":
            return self.GF.getSymbolType(name)
        elif frame == "LF@":
            if len(self.LF) == 0:
                sys.stderr.write("there is no local frame in frame stack\n")
                exit(55)
            else:
                return self.LF[len(self.LF)-1].getSymbolType(name)
        elif frame == "TF@":
            return self.TF.getSymbolType(name)
        else:
            sys.stderr.write("trying to add var to unknown frame: "+name+" "+frame)
            exit(70)


    def getVarValue(self, frame, name):
        """
        Get value of variable
        """
        if frame == "GF@":
            return self.GF.getSymbolValue(name)
        elif frame == "LF@":
            if len(self.LF) == 0:
                sys.stderr.write("there is no local frame in frame stack\n")
                exit(55)
            else:
                return self.LF[len(self.LF)-1].getSymbolValue(name)
        elif frame == "TF@":
            return self.TF.getSymbolValue(name)
        else:
            sys.stderr.write("trying to add var to unknown frame: "+name+" "+frame)
            exit(70)

    def createFrame(self):
        """
        Implements CREATEFRAME
        """
        self.TF.variables.clear()
        self.TFdefinied = True

    def pushFrame(self):
        """
        Implements FUSHFRAME
        """
        self.LF.append(copy.deepcopy(self.TF))
        self.TF.variables.clear()
        self.TFdefinied = False

    def popFrame(self):
        """
        Implements POPFRAME
        """
        if len(self.LF) == 0:
            sys.stderr.write("can't pop empty frame\n")
            exit(56)
        self.TF = copy.deepcopy(self.LF[len(self.LF)-1])
        self.LF.pop()
        self.TFdefinied = True

    def stackPush(self, value, type1):
        """
        Implements PUSHS
        """
        self.stack.append(Const(value, type1))


    def stackPop(self):
        """
        Implements POPS
        """
        if len(self.stack) == 0:
            sys.stderr.write("can't pop empty stack\n")
            exit(56)           
        self.stack.pop()

    def stackTopValue(self):
        """
        Implements get value from top of stack
        """
        if len(self.stack) == 0:
            sys.stderr.write("can't get top value of empty stack\n")
            exit(56)
        ret = self.stack[len(self.stack)-1]
        return ret.value

    def stackTopType(self):
        """
        Implements get type from top of stack
        """
        if len(self.stack) == 0:
            sys.stderr.write("can't get top type of empty stack\n")
            exit(56)
        ret = self.stack[len(self.stack)-1]
        return ret.definiedType